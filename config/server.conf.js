'use strict';

module.exports = {
    development: {
        localAddress:  'http://127.0.0.1:3000', // IP & PORT - required
        publicAddress: 'http://localhost:3000'
    },
    production: {
        localAddress:  'http://127.0.0.1:3000', // IP & PORT - required
        publicAddress: 'https://stormy-plains-49876.herokuapp.com'
    }
}
