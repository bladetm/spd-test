'use strict';

const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const FlowBabelWebpackPlugin = require('flow-babel-webpack-plugin');

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: [
        'whatwg-fetch',
        'babel-polyfill',
        './src/index'
    ],
    output: {
        path: path.resolve('./dist'),
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: 'style-loader!raw-loader'
            }
        ]
    },
    plugins: [
        new FlowBabelWebpackPlugin({sourceMap: true}),
        new UglifyJSPlugin()
    ]
}
