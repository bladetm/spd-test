'use strict';

const url    = require('url');
const http   = require('http');
const app    = require('./server');
const config = require('./config/server.conf');

const env     = app.get('env');
const address = url.parse(config[env].localAddress);

app.set('port', address.port);

const server = http.createServer(app);
server.listen(address.port);
server.on('error', onError);
server.on('listening', onListening);


function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    switch (error.code) {
        case 'EACCES':
            console.error(`Port ${address.port} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`Port ${address.port} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}


function onListening() {
    console.info(`Environment: ${env.toUpperCase()}.`);
    console.info(`Visit ${config[env].publicAddress}/`);
}
