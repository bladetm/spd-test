'use strict';

const fs   = require('fs');
const path = require('path');


class FakeDB {

    // ADDRESS DATA

    getCountries() {
        return this._readFile('countries');
    }

    getProvincesByCountryId(countryId) {
        return this._readFile('provinces')
            .then(provinces => {
                return provinces.filter(province => province.country_id === + countryId);
            });
    }

    getCitiesByProvinceId(provinceId) {
        return this._readFile('cities')
            .then(cities => {
                return cities.filter(city => city.province_id === + provinceId);
            });
    }

    // OFFICES

    getOffices() {
        return this._readFile('offices');
    }

    updateOffice(changedOffice) {
        let officeList;

        return this.getOffices()
            .then(offices => {
                officeList = offices;

                return offices.findIndex(office => office.id === changedOffice.id);
            })
            .then(index => {
                if (index === -1) {
                    throw new Error(`Office with id ${changedOffice.id} is undefined`);
                }
                officeList.splice(index, 1, changedOffice);

                return this._writeFile('offices', officeList);
            })
            .then(() => changedOffice);
    }

    addOffice(newOffice) {
        return this.getOffices()
            .then(offices => {
                offices.push(newOffice);

                return this._writeFile('offices', offices);
            })
            .then(() => newOffice);
    }

    removeOffice(officeId) {
        let officeList;

        return this.getOffices()
            .then(offices => {
                officeList = offices;

                return offices.findIndex(office => office.id === officeId);
            })
            .then(index => {
                if (index === -1) {
                    throw new Error(`Office with id ${changedOffice.id} is undefined`);
                }
                officeList.splice(index, 1);

                return this._writeFile('offices', officeList);
            });
    }

    // FS

    _readFile (fileName) {
        const filePath = path.resolve(`./server/data/${fileName}.json`);

        return new Promise((resolve, reject) => {
            fs.readFile(
                filePath,
                'utf8',
                (err, data) => err
                    ? reject(err)
                    : resolve(JSON.parse(data))
            );
        });
    }

    _writeFile (fileName, data) {
        const filePath = path.resolve(`./server/data/${fileName}.json`);

        if (!data || data.constructor !== String) {
            data = JSON.stringify(data);
        }

        return new Promise((resolve, reject) => {
            fs.writeFile(
                filePath,
                data,
                'utf8',
                (err, data) => err
                    ? reject(err)
                    : resolve()
            );
        });
    }
}

module.exports = new FakeDB();
