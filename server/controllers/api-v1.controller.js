'use strict';

const fakeDB = require('../services/fake-db');


class ApiV1Controller {
    getCountries(req, res, next) {
        fakeDB.getCountries()
            .then(countries => res.json({countries}))
            .catch(err => next(err));
    }

    getProvince(req, res, next) {
        let { id } = req.params;

        fakeDB.getProvincesByCountryId(id)
            .then(provinces => res.json({provinces}))
            .catch(err => next(err));
    }

    getCity(req, res, next) {
        let { id } = req.params;

        fakeDB.getCitiesByProvinceId(id)
            .then(cities => res.json({cities}))
            .catch(err => next(err));
    }

    getOffices(req, res, next) {
        fakeDB.getOffices()
            .then(offices => res.json({offices}))
            .catch(err => next(err));
    }

    putOffices(req, res, next) {
        let errors = validateOfficeBody(req.body);

        if (errors) {
            res.status(400).json(errors);
            return;
        }

        fakeDB.updateOffice(req.body)
            .then(result => res.status(200).send(result))
            .catch(err => next(err));
    }

    postOffices(req, res, next) {
        let errors = validateOfficeBody(req.body);

        if (errors) {
            res.status(400).json(errors);
            return;
        }

        fakeDB.addOffice(req.body)
            .then(result => res.status(200).send(result))
            .catch(err => next(err));
    }

    deleteOffices(req, res, next) {
        let { id, reason, notes } = req.body;

        // here we can send this data to email, save to db etc
        console.log(`OFFICE DELETED. REASON: ${reason}, ${notes}`);

        fakeDB.removeOffice(id)
            .then(result => res.status(200).send({}))
            .catch(err => next(err));
    }
}

module.exports = new ApiV1Controller();


function validateOfficeBody(body) {
    let {
        country,
        province,
        city,
        'postal-code': postalCode,
        street
    } = body;

    let errors = [];
    let requiredFields = { country, province, city, postalCode, street };

    for (let key in requiredFields) {
        if (!requiredFields[key]) {
            if (key === 'postalCode') {
                key = 'postal-code';
            }
            key = `${key}_error`;

            errors.push({[key]: 'This field is required'});
            continue;
        }

        if (requiredFields[key].constructor === Object && requiredFields[key].id === undefined) {
            key = `${key}_error`;
            errors.push({[key]: 'This field is required'});
        }
    }

    return errors.length ? errors : null;
}
