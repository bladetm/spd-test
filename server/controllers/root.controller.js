'use strict';

const path = require('path');

class RootController {
    homePage(req, res, next) {
        res.redirect('/company-info');
    }

    companyPage(req, res, next) {
        const filePath = path.resolve('./dist/html/company-info.html');
        res.sendFile(filePath);
    }
}

module.exports = new RootController();
