'use strict';

const http = require('http');

class ErrorController {
    httpCodeToError(code, req, res, next) {
        if (code.constructor !== Number) {
            return next(code);
        }

        next({
            code,
            message: http.STATUS_CODES[code] || `Unhandled http status code ${code}`
        });
    }

    errorHandler(err, req, res, next) {
        const now = new Date().toISOString();
        const env = req.app.get('env');

        if (err instanceof Error) {
            console.error(`[${now}] INTERNAL SERVER ERROR:`, err);

            if (env === 'production') {
                err = {
                    code: 500,
                    message: 'Internal server error!'
                }
            }
        }

        const isHttpCode = err.code in http.STATUS_CODES;
        res.status(isHttpCode ? err.code : 500)
           .send(`
                <h1>Error ${err.code}</h1>
                <h3>${err.message}</h3>
                <p>${err.stack || ''}</p>
           `);
    }
}

module.exports = new ErrorController();
