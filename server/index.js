'use strict';

const path         = require('path');
const express      = require('express');
const morgan       = require('morgan');
const bodyParser   = require('body-parser');

const app = express();
const env = app.get('env');

// SERVER

app.use(bodyParser.json());
app.use(express.static(path.resolve('./dist')));

if (env === 'development') {
    app.use(morgan(`[${env}] :date[iso] :status :method :url :response-time ms`));
}

// routing
app.use(require('./router'));


module.exports = app;
