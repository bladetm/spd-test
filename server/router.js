'use strict';

const express           = require('express');
const rootCtrl          = require('./controllers/root.controller');
const errorCtrl         = require('./controllers/error.controller');
const v1Ctrl            = require('./controllers/api-v1.controller');
const lastSlashRedirect = require('./middlewares/last-slash-redirect');

const router = express.Router({ strict: true, caseSensitive: true });

// ROUTES
router.use(lastSlashRedirect);
router.get('/',             rootCtrl.homePage);
router.get('/company-info', rootCtrl.companyPage);

// API

router.get('/api/v1/countries',     v1Ctrl.getCountries);
router.get('/api/v1/provinces/:id', v1Ctrl.getProvince);
router.get('/api/v1/cities/:id',    v1Ctrl.getCity);

router   .get('/api/v1/offices',      v1Ctrl.getOffices);
router   .put('/api/v1/offices',      v1Ctrl.putOffices);
router  .post('/api/v1/offices',      v1Ctrl.postOffices);
router.delete('/api/v1/offices',      v1Ctrl.deleteOffices);

// ERRORS
router.use((req, res, next) => next(404));
router.use(errorCtrl.httpCodeToError);
router.use(errorCtrl.errorHandler);

module.exports = router;
