'use strict';

module.exports = (req, res, next) => {
    const regex = /\/$/;

    if (req.path !== '/' && regex.test(req.path)) {
        return res.redirect(301, req.path.replace(regex, ''));
    }

    next();
}
