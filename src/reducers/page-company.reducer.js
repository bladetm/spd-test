// @flow

import officeFactory from '../store/officeFactory';

import {
    OFFICE_ADD,
    OFFICE_EDIT,
    OFFICE_EDIT_CANCEL,
    OFFICE_UPDATE,
    OFFICE_SAVE,
    OFFICE_SAVE_SUCCESS,
    OFFICE_SAVE_ERROR,
    OFFICE_CLEAR_VALIDATION,
    OFFICE_CONFIRM_DELETE,
    OFFICE_CONFIRM_DELETE_UPDATE,
    OFFICE_CONFIRM_DELETE_CANCEL,
    OFFICE_DELETE,
    OFFICE_DELETE_SUCCESS,

    FETCH_DATA,
    FETCH_DATA_SUCCESS,
    FETCH_PROVINCES,
    FETCH_PROVINCES_SUCCESS,
    FETCH_CITIES,
    FETCH_CITIES_SUCCESS
} from '../actions/action-list';

import type {
    companyOfficesPageDataType,
    reduxActionType
} from '../store/store-types';


const officesDefaultState: companyOfficesPageDataType = {
    pageName:    'Company info',
    sectionName: 'Offices',
    loading:     false,
    frozen:      false,
    removingData: {
        reasons:  ['Former record', 'Duplicate record', 'Record never existed', 'Other'],
        officeId: null,
        reason:   '',
        notes:    ''
    },
    data: {
        countries: [],
        provinces: [],
        cities:    [],
        offices:   []
    }
}

export function officesReducer(state: companyOfficesPageDataType = officesDefaultState, action: reduxActionType): companyOfficesPageDataType {
    switch (action.type) {

        // FETCH

        case FETCH_DATA:
            return {...state, loading: true};

        case FETCH_PROVINCES:
            return {
                ...state,
                data: {...state.data, provinces: [], cities: []}
            };

        case FETCH_CITIES:
            return {
                ...state,
                data: {...state.data, cities: []}
            };

        case FETCH_DATA_SUCCESS:
            return {
                ...state,
                loading: false,
                data: {...state.data, ...action.payload}
            };

        case FETCH_PROVINCES_SUCCESS:
        case FETCH_CITIES_SUCCESS:
            return {
                ...state,
                data: { ...state.data, ...action.payload }
            };

        // OFFICE

        case OFFICE_ADD: {
            const newState = {
                ...state,
                data: {
                    ...state.data,
                    ...action.payload
                }
            };
            const office = officeFactory(
                newState.data.countries[0],
                newState.data.provinces[0],
                newState.data.cities[0]
            );

            newState.data.offices.unshift(office);

            return newState;
        }

        case OFFICE_EDIT: {
            const index = state.data.offices.findIndex(
                office => office.id === action.payload
            );

            const officeBackup = {...state.data.offices[index]};
            const newState = {
                ...state,
                data: {...state.data, officeBackup}
            };

            const office = {...state.data.offices[index], editing: true};
            newState.data.offices.splice(index, 1, office);

            return newState;
        }

        case OFFICE_EDIT_CANCEL: {
            let newState;

            if (state.data.officeBackup) { // edit
                const office = {...state.data.officeBackup};
                const index  = state.data.offices.findIndex(
                    officeItem => officeItem.id === office.id
                );

                newState = {
                    ...state,
                    data: {...state.data, officeBackup: null}
                };
                newState.data.offices.splice(index, 1, office);
            } else { // empty office (new)
                newState = {...state};
                newState.data.offices.shift();
            }

            return newState;
        }

        case OFFICE_UPDATE: {
            const index = state.data.offices.findIndex(
                office => office.id === action.payload.id
            );

            const newState = {...state};

            Object.keys(action.payload)
                .forEach(key => {
                    switch(key) {
                        case 'country': {
                            const office = {...state.data.offices[index], ...action.payload, province: {}};
                            newState.data.offices.splice(index, 1, office);
                            break;
                        }

                        case 'province': {
                            const office = {...state.data.offices[index], ...action.payload, city: {}};
                            newState.data.offices.splice(index, 1, office);
                            break;
                        }

                        default: {
                            const office = {...state.data.offices[index], ...action.payload};
                            newState.data.offices.splice(index, 1, office);
                        }
                    }
                });

            return newState;
        }

        case OFFICE_SAVE:
            return {...state, frozen: true}

        case OFFICE_SAVE_SUCCESS: {
            const newState = {
                ...state,
                frozen: false,
                data: {
                    ...state.data,
                    officeBackup: null
                }
            };

            const index = newState.data.offices.findIndex(
                officeItem => officeItem.id === action.payload.id
            );
            newState.data.offices.splice(index, 1, action.payload);

            return newState;
        }

        case OFFICE_SAVE_ERROR: {
            const newState = {...state, frozen: false};

            const index  = newState.data.offices.findIndex(officeItem => officeItem.editing);
            const office = {
                ...newState.data.offices[index],
                ...action.payload,
            };

            newState.data.offices.splice(index, 1, office);

            return newState;
        }

        case OFFICE_CLEAR_VALIDATION: {
            const newState = {...state};
            const index    = newState.data.offices.findIndex(office => office.id === action.payload.officeId);
            const office   = {...newState.data.offices[index], ...action.payload.value};

            newState.data.offices.splice(index, 1, office);

            return newState;
        }

        case OFFICE_CONFIRM_DELETE:
            return {
                ...state,
                removingData: {
                    ...state.removingData,
                    officeId: action.payload
                }
            };

        case OFFICE_CONFIRM_DELETE_UPDATE:
            return {
                ...state,
                removingData: {
                    ...state.removingData,
                    ...action.payload
                }
            }

        case OFFICE_CONFIRM_DELETE_CANCEL:
            return {
                ...state,
                removingData: {
                    ...state.removingData,
                    officeId: null,
                    reason:   '',
                    notes:    ''
                }
            };

        case OFFICE_DELETE:
            return {...state, frozen: true};

        case OFFICE_DELETE_SUCCESS: {
            const newState = {...state, frozen: false};
            const index = newState.data.offices.findIndex(
                office => office.id === action.payload
            );

            newState.data.offices.splice(index, 1);

            return newState;
        }

    }

    return state;
}
