// @flow

import thunk  from 'redux-thunk';
import logger from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import { officesReducer } from '../reducers/page-company.reducer';

import type { companyOfficesPageDataType } from './store-types';


export default function configStore(initialStore: any): companyOfficesPageDataType {
    return createStore(officesReducer, initialStore, applyMiddleware(thunk, logger));
}
