// @flow

import type {
    countryType,
    provinceType,
    cityType,
    officeType
} from './store-types';

export default function officeFactory(country: countryType, province: provinceType, city: cityType): officeType {
    return {
        id: (Math.random() * 1e17).toString(36),
        country,
        province,
        city,
        "postal-code": 0,
        street:  '',
        primary: false,
        editing: true
    }
}
