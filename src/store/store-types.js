// @flow

export type countryType = {
    +id:   number,
    +name: string
}

export type provinceType = {
    +id:         number,
    +country_id: number,
    +name:       string
}

export type cityType = {
    +id:          number,
    +province_id: number,
    +name:        string
}

export type officeType = {
    +id:                   number|string,
    +country:              countryType,
    +country_error?:       string,
    +province:             provinceType,
    +province_error?:      string,
    +city:                 cityType,
    +city_error?:          string,
    +"postal-code":        number,
    +"postal-code_error"?: string,
    +street:               string,
    +street_error?:        string,
    +address2?:            string,
    +phone?:               string,
    +fax?:                 string,
    +email?:               string,
    +primary:              boolean,
    +editing?:             boolean
}

export type officeRemovingDataType = {
    reasons:  Array<string>,
    officeId: ?number,
    reason:   ?string,
    notes:    string
}

export type companyOfficesPageDataType = {
    +pageName:     string,
    +sectionName:  string,
    +loading:      boolean, // need before site loaded
    +frozen:       boolean, // need on every post/put request
    +removingData: officeRemovingDataType,
    +data: {
        +countries:     Array<countryType>,
        +provinces:     Array<provinceType>,
        +cities:        Array<cityType>,
        +offices:       Array<officeType>,
        +officeBackup?: ?officeType
    }
}

export type reduxActionType = {
    type:    string,
    payload: any
}
