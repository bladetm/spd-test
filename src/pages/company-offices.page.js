// @flow

import React from 'react';
import { Button } from 'react-bootstrap';

import OfficeViewComponent   from '../components/office-view/office-view.component';
import OfficeEditComponent   from '../components/office-edit/office-edit.component';
import OfficeDeleteComponent from '../components/office-delete/office-delete.component';

import type { companyOfficesPageDataType, officeType } from '../store/store-types';

import './pages.css';
import './company-offices.page.css';


export default class CompanyOfficesPage extends React.Component {
    constructor(props: {pageData: companyOfficesPageDataType, actions: any}) {
        super(props);
    }

    render() {
        const pageData: companyOfficesPageDataType = this.props.pageData;

        return (
            <section className="page page-company-offices">
                <header className="page-header">
                    <h1>{pageData.sectionName}</h1>
                    <h2>{pageData.pageName}</h2>
                </header>
                <div className="page-description">
                    Updating your location and contact information helps you appeal to regional investors and service providers.
                </div>

                <div className="page-body">
                    <div className="float-root">
                        <Button
                            id="createOffice"
                            className="office-style"
                            onClick={() => this.onAddButtonClick()}
                            disabled={this.props.pageData.frozen} >
                            Add New Office
                        </Button>
                        <span className="pull-right">{pageData.data.offices.length} Offices</span>
                    </div>

                    <div className="offices-container">
                        {pageData.data.offices.length
                            ? pageData.data.offices.map(office => this.renderOfficeComponent(office))
                            : <div className="no-offices-label">No data to show</div>
                        }
                    </div>
                </div>

                <footer className="page-footer">
                    <Button href="#">Back</Button>

                    <Button href="#" bsStyle="link">
                        <span className="icon">+</span>
                        Provide additional comments
                    </Button>

                    <div className="page-footer-button-block">
                        <Button href="#">Skip this step</Button>
                        <Button href="#" bsStyle="primary">Continue</Button>
                    </div>
                </footer>

                <OfficeDeleteComponent
                    data={this.props.pageData.removingData}
                    actions={this.props.actions}
                    isPageFrozen={this.props.pageData.frozen} />
            </section>
        );
    }

    renderOfficeComponent(officeData: officeType) {
        const data      = this.props.pageData.data;
        const component = officeData.editing
            ? (<OfficeEditComponent
                    officeData={officeData}
                    key={officeData.id.toString()}
                    actions={this.props.actions}
                    countries={data.countries}
                    provinces={data.provinces}
                    cities={data.cities}
                    isPageFrozen={this.props.pageData.frozen} />)
            : (<OfficeViewComponent
                    officeData={officeData}
                    key={officeData.id.toString()}
                    actions={this.props.actions}
                    isPageFrozen={this.props.pageData.frozen} />);

        return component;
    }

    onAddButtonClick() {
        if (this.props.pageData.frozen) {
            return;
        }

        this.props.actions.officeAdd();
    }
}
