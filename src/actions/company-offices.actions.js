// @flow

import {
    OFFICE_ADD,
    OFFICE_EDIT,
    OFFICE_EDIT_CANCEL,
    OFFICE_UPDATE,
    OFFICE_SAVE,
    OFFICE_SAVE_SUCCESS,
    OFFICE_SAVE_ERROR,
    OFFICE_CLEAR_VALIDATION,
    OFFICE_CONFIRM_DELETE,
    OFFICE_CONFIRM_DELETE_UPDATE,
    OFFICE_CONFIRM_DELETE_CANCEL,
    OFFICE_DELETE,
    OFFICE_DELETE_SUCCESS,

    FETCH_DATA,
    FETCH_DATA_SUCCESS,
    FETCH_PROVINCES,
    FETCH_PROVINCES_SUCCESS,
    FETCH_CITIES,
    FETCH_CITIES_SUCCESS
} from './action-list';


// office

export function officeAdd() {
    return (dispatch: Function, getState: Function) => {
        const state = getState();
        const someOfficeInEditing = state.data.offices.find(office => office.editing);

        if (someOfficeInEditing) {
            dispatch({ type: OFFICE_EDIT_CANCEL })
        }

        const data = {};

        fetch(`/api/v1/provinces/${state.data.countries[0].id}`)
            .then(res => res.json())
            .then(json => {
                data.provinces = json.provinces;

                return fetch(`/api/v1/cities/${json.provinces[0].id}`);
            })
            .then(res => res.json())
            .then(json => {
                data.cities = json.cities;

                dispatch({
                    type: OFFICE_ADD,
                    payload: data
                });
            })
            .catch(err => console.error(err));
    }
}

export function officeEdit(officeId: number) {
    return (dispatch: Function, getState: Function) => {
        const state = getState();
        const someOfficeInEditing = state.data.offices.find(office => office.editing);

        if (someOfficeInEditing) {
            dispatch({ type: OFFICE_EDIT_CANCEL })
        }

        dispatch({
            type: OFFICE_EDIT,
            payload: officeId
        });
    }
}

export function officeEditCancel() {
    return { type: OFFICE_EDIT_CANCEL }
}

export function officeUpdate(updatedValue: any) {
    return {
        type: OFFICE_UPDATE,
        payload: updatedValue
    }
}

export function officeSave() {
    return (dispatch: Function, getState: Function) => {
        dispatch({ type: OFFICE_SAVE });

        let response;
        const state  = getState();
        const office = state.data.offices.find(office => office.editing);
        const body   = {...office, editing: false};
        const method = (state.data.officeBackup && state.data.officeBackup.id) ? 'PUT' : 'POST';

        fetch('/api/v1/offices', {
            method,
            body: JSON.stringify(body),
            headers: {'Content-Type': 'application/json'}
        })
        .then(res => {
            response = res;
            return res.json();
        })
        .then(json => {
            if ((response.status + '').slice(0,1) === '2') {
                dispatch({
                    type: OFFICE_SAVE_SUCCESS,
                    payload: json
                });
            } else {
                const payload = {};

                json.forEach(err => {
                    const key = Object.keys(err)[0];
                    payload[key] = err[key];
                });

                dispatch({
                    type: OFFICE_SAVE_ERROR,
                    payload: payload
                });
            }
        })
        .catch(err => console.error(err));
    }
}

export function officeClearValidation(officeId: number, validationKey: string) {
    return {
        type: OFFICE_CLEAR_VALIDATION,
        payload: {
            officeId,
            value: {[validationKey]: null}
        }
    }
}

export function officeConfirmDelete(officeId: number) {
    return (dispatch: Function, getState: Function) => {
        const state = getState();
        const someOfficeInEditing = state.data.offices.find(office => office.editing);

        if (someOfficeInEditing) {
            dispatch({ type: OFFICE_EDIT_CANCEL })
        }

        dispatch({
            type: OFFICE_CONFIRM_DELETE,
            payload: officeId
        });
    }
}

export function officeConfirmDeleteUpdate(updatedValue: any) {
    return {
        type: OFFICE_CONFIRM_DELETE_UPDATE,
        payload: updatedValue
    }
}

export function officeConfirmDeleteCancel(officeId: number) {
    return { type: OFFICE_CONFIRM_DELETE_CANCEL };
}

export function officeDelete(officeId: number, reason: string, notes: string) {
    return (dispatch: Function, getState: Function) => {
        dispatch({ type: OFFICE_DELETE });

        const body = {
            id: officeId,
            reason,
            notes
        }

        fetch('/api/v1/offices', {
            method: 'DELETE',
            body: JSON.stringify(body),
            headers: {'Content-Type': 'application/json'}
        })
        .then(() => {
            dispatch({ type: OFFICE_CONFIRM_DELETE_CANCEL });

            dispatch({
                type: OFFICE_DELETE_SUCCESS,
                payload: officeId
            });
        })
        .catch(err => console.error(err));
    }
}

// fetch

export function fetchData() {
    return (dispatch: Function) => {
        const data = {};

        dispatch({ type: FETCH_DATA });

        fetch('/api/v1/countries')
            .then(res => res.json())
            .then(json => {
                data.countries = json.countries;

                return fetch(`/api/v1/offices`);
            })
            .then(res => res.json())
            .then(json => {
                data.offices = json.offices;

                dispatch({
                    type: FETCH_DATA_SUCCESS,
                    payload: data
                });
            })
            .catch(err => console.error(err));
    }
}

export function fetchProvincesByCountryId(countryId: number, needLoadFirstCity: ?boolean) {
    return (dispatch: Function) => {
        dispatch({ type: FETCH_PROVINCES });

        fetch(`/api/v1/provinces/${countryId}`)
            .then(res => res.json())
            .then(json => {
                dispatch({
                    type: FETCH_PROVINCES_SUCCESS,
                    payload: json
                });

                if (needLoadFirstCity) {
                    fetchCitiesByProvinceId(json.provinces[0].id);
                }
            })
            .catch(err => console.error(err));
    }
}

export function fetchCitiesByProvinceId(provinceId: number) {
    return (dispatch: Function) => {
        dispatch({ type: FETCH_CITIES });

        fetch(`/api/v1/cities/${provinceId}`)
            .then(res => res.json())
            .then(json => {
                dispatch({
                    type: FETCH_CITIES_SUCCESS,
                    payload: json
                });
            })
            .catch(err => console.error(err));
    }
}
