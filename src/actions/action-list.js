// @flow

// Company page - fetch data
export const FETCH_DATA              = 'FETCH_DATA';
export const FETCH_DATA_SUCCESS      = 'FETCH_DATA_SUCCESS';
export const FETCH_PROVINCES         = 'FETCH_PROVINCES';
export const FETCH_PROVINCES_SUCCESS = 'FETCH_PROVINCES_SUCCESS';
export const FETCH_CITIES            = 'FETCH_CITIES';
export const FETCH_CITIES_SUCCESS    = 'FETCH_CITIES_SUCCESS';

// Company page - office

export const OFFICE_ADD                   = 'OFFICE_ADD';
export const OFFICE_EDIT                  = 'OFFICE_EDIT';
export const OFFICE_EDIT_CANCEL           = 'OFFICE_EDIT_CANCEL';
export const OFFICE_UPDATE                = 'OFFICE_UPDATE';
export const OFFICE_SAVE                  = 'OFFICE_SAVE';
export const OFFICE_SAVE_SUCCESS          = 'OFFICE_SAVE_SUCCESS';
export const OFFICE_SAVE_ERROR            = 'FETCH_CITIES_ERROR';
export const OFFICE_CLEAR_VALIDATION      = 'OFFICE_CLEAR_VALIDATION';
export const OFFICE_CONFIRM_DELETE        = 'OFFICE_CONFIRM_DELETE';
export const OFFICE_CONFIRM_DELETE_UPDATE = 'OFFICE_CONFIRM_DELETE_UPDATE';
export const OFFICE_CONFIRM_DELETE_CANCEL = 'OFFICE_CONFIRM_DELETE_CANCEL';
export const OFFICE_DELETE                = 'OFFICE_DELETE';
export const OFFICE_DELETE_SUCCESS        = 'OFFICE_DELETE_SUCCESS';
