// @flow

import React        from 'react';
import ReactDom     from 'react-dom';
import { Provider } from 'react-redux';

import App          from './components/app/app.component';
import configStore  from './store/config-store';

import type { companyOfficesPageDataType } from './store/store-types';

const store: companyOfficesPageDataType = configStore();

ReactDom.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.querySelector('#root')
);
