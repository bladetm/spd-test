import React from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';

import './sidebar.component.css';

export default class SidebarComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <nav className="sidebar">
                <div className="step">
                    <img className="step-image" src="images/steps.svg" alt="Step 2 of 8"/>
                </div>
                <ListGroup className="side-menu">
                    <ListGroupItem>
                        <a className="side-menu-lnk" href="#">Company info</a>
                        <ListGroup className="side-submenu">
                            <ListGroupItem><span className="side-submenu-section checked">Basic info</span></ListGroupItem>
                            <ListGroupItem><span className="side-submenu-section active">Offices</span></ListGroupItem>
                            <ListGroupItem><span className="side-submenu-section">Competitors</span></ListGroupItem>
                        </ListGroup>
                    </ListGroupItem>
                    <ListGroupItem><a className="side-menu-lnk" href="#">My firm</a></ListGroupItem>
                    <ListGroupItem><a className="side-menu-lnk" href="#">Deals</a></ListGroupItem>
                    <ListGroupItem><a className="side-menu-lnk" href="#">Financials</a></ListGroupItem>
                </ListGroup>
            </nav>
        );
    }
}
