// @flow

import React  from 'react';
import Select from 'react-select';
import { Button, FormGroup, FormControl } from 'react-bootstrap';

import CheckboxComponent from '../checkbox/checkbox.component';
import type {
    officeType,
    countryType,
    provinceType,
    cityType
} from '../../store/store-types';

type propsType = {
    officeData:   officeType,
    actions:      any,
    countries:    Array<countryType>,
    provinces:    Array<provinceType>,
    cities:       Array<cityType>,
    isPageFrozen: boolean
}

export default class OfficeEditComponent extends React.Component {
    constructor(props: propsType) {
        super(props);
    }

    componentDidMount() {
        const countryId  = this.props.officeData.country.id;
        const provinceId = this.props.officeData.province.id;
        this.props.actions.fetchProvincesByCountryId(countryId);
        this.props.actions.fetchCitiesByProvinceId(provinceId);
    }

    render() {
        const officeData: officeType = this.props.officeData;

        return (
            <form className="office-view editing">
                <div className="office-view-block">
                    <FormGroup controlId="country" className="office-view-row">
                        <div className="office-view-title">*Country:</div>
                        <div className="office-view-description">
                            <Select
                                clearable={false}
                                value={officeData.country.id}
                                options={this.prepareListToSelect(this.props.countries)}
                                onChange={selectedData => this.onSelect('country', 'countries', selectedData)}
                                onFocus={() => this.onInputFocus('country_error')}
                                disabled={this.props.isPageFrozen} />
                            { officeData.country_error && (
                                <span className="text-danger validation">
                                    {officeData.country_error}
                                </span>
                            )}
                        </div>
                    </FormGroup>

                    <FormGroup controlId="province" className="office-view-row">
                        <div className="office-view-title">*State/Province:</div>
                        <div className="office-view-description">
                            <Select
                                clearable={false}
                                value={officeData.province.id}
                                options={this.prepareListToSelect(this.props.provinces)}
                                onChange={selectedData => this.onSelect('province', 'provinces', selectedData)}
                                onFocus={() => this.onInputFocus('province_error')}
                                disabled={this.props.isPageFrozen} />
                            { officeData.province_error && (
                                <span className="text-danger validation">
                                    {officeData.province_error}
                                </span>
                            )}
                        </div>
                    </FormGroup>

                    <FormGroup controlId="postalCode" className="office-view-row">
                        <div className="office-view-title">*Postal code:</div>
                        <div className="office-view-description">
                            <FormControl
                                type="text"
                                value={officeData['postal-code']}
                                className={this.isFieldFilled(officeData['postal-code'])}
                                onChange={e => this.onChangeData('postal-code', e.target.value)}
                                onFocus={() => this.onInputFocus('postal-code_error')}
                                disabled={this.props.isPageFrozen} />
                            { officeData['postal-code_error'] && (
                                <span className="text-danger validation">
                                    {officeData['postal-code_error']}
                                </span>
                            )}
                        </div>
                    </FormGroup>

                    <FormGroup controlId="city" className="office-view-row">
                        <div className="office-view-title">*City:</div>
                        <div className="office-view-description">
                            <Select
                                clearable={false}
                                value={officeData.city.id}
                                options={this.prepareListToSelect(this.props.cities)}
                                onChange={selectedData => this.onSelect('city', 'cities', selectedData)}
                                onFocus={() => this.onInputFocus('city_error')}
                                disabled={this.props.isPageFrozen} />
                            { officeData.city_error && (
                                <span className="text-danger validation">
                                    {officeData.city_error}
                                </span>
                            )}
                        </div>
                    </FormGroup>

                    <FormGroup controlId="street" className="office-view-row">
                        <div className="office-view-title">*Street Address:</div>
                        <div className="office-view-description">
                            <FormControl
                                type="text"
                                value={officeData.street}
                                className={this.isFieldFilled(officeData.street)}
                                onChange={e => this.onChangeData('street', e.target.value)}
                                onFocus={() => this.onInputFocus('street_error')}
                                disabled={this.props.isPageFrozen} />
                            { officeData.street_error && (
                                <span className="text-danger validation">
                                    {officeData.street_error}
                                </span>
                            )}
                        </div>
                    </FormGroup>

                    <FormGroup controlId="address2" className="office-view-row">
                        <div className="office-view-title">Address 2:</div>
                        <div className="office-view-description">
                            <FormControl
                                type="text"
                                value={officeData.address2}
                                className={this.isFieldFilled(officeData.address2)}
                                onChange={e => this.onChangeData('address2', e.target.value)}
                                disabled={this.props.isPageFrozen} />
                        </div>
                    </FormGroup>
                </div>

                <div className="office-view-block">
                    <FormGroup controlId="phone" className="office-view-row">
                        <div className="office-view-title">Phone:</div>
                        <div className="office-view-description">
                            <FormControl
                                type="text"
                                value={officeData.phone}
                                className={this.isFieldFilled(officeData.phone)}
                                onChange={e => this.onChangeData('phone', e.target.value)}
                                disabled={this.props.isPageFrozen} />
                        </div>
                    </FormGroup>

                    <FormGroup controlId="fax" className="office-view-row">
                        <div className="office-view-title">Fax:</div>
                        <div className="office-view-description">
                            <FormControl
                                type="text"
                                value={officeData.fax}
                                className={this.isFieldFilled(officeData.fax)}
                                onChange={e => this.onChangeData('fax', e.target.value)}
                                disabled={this.props.isPageFrozen} />
                        </div>
                    </FormGroup>

                    <FormGroup controlId="email" className="office-view-row">
                        <div className="office-view-title">Email:</div>
                        <div className="office-view-description">
                            <FormControl
                                type="text"
                                value={officeData.email}
                                className={this.isFieldFilled(officeData.email)}
                                onChange={e => this.onChangeData('email', e.target.value)}
                                disabled={this.props.isPageFrozen} />
                        </div>
                    </FormGroup>

                    <FormGroup controlId="primary" className="office-view-row">
                        <div className="office-view-title">Office Type:</div>
                        <div className="office-view-description">
                            <CheckboxComponent
                                value={officeData.primary}
                                label="Primary HQ"
                                onChange={e => this.onChangeData('primary', e.target.checked)}
                                disabled={this.props.isPageFrozen} />
                        </div>
                    </FormGroup>
                </div>

                <div className="office-view-block">
                    <div className="pull-right">
                        <Button
                            bsSize="small"
                            onClick={() => this.onCancelClick()}
                            disabled={this.props.isPageFrozen} >
                            Cancel
                        </Button>
                        <Button
                            bsSize="small"
                            bsStyle="primary"
                            onClick={() => this.onSaveClick()}
                            disabled={this.props.isPageFrozen} >
                            Save
                        </Button>
                    </div>
                </div>
            </form>
        );
    }

    isFieldFilled(fieldData: any) {
        return (fieldData === null || fieldData === undefined || fieldData === '') ? '' : 'filled';
    }

    onChangeData(key: string, value: any) {
        if (this.props.isPageFrozen) {
            return;
        }

        this.props.actions.officeUpdate({
            id: this.props.officeData.id,
            [key]: value
        });
    }

    onSelect(officeDataKey: string, selectDataKey: string, selectedData: any) {
        if (this.props.isPageFrozen) {
            return;
        }

        const selectedItem: any = this.props[selectDataKey].find(item => item.id === selectedData.value);
        this.onChangeData(officeDataKey, selectedItem);

        switch (officeDataKey) {
            case 'country':
                this.props.actions.fetchProvincesByCountryId(selectedData.value, true);
                break;

            case 'province':
                this.props.actions.fetchCitiesByProvinceId(selectedData.value);
                break;
        }
    }

    onCancelClick() {
        if (this.props.isPageFrozen) {
            return;
        }

        this.props.actions.officeEditCancel();
    }

    onSaveClick() {
        if (this.props.isPageFrozen) {
            return;
        }

        this.props.actions.officeSave();
    }

    onInputFocus(validationKey: string) {
        if (this.props.isPageFrozen) {
            return;
        }

        this.props.actions.officeClearValidation(this.props.officeData.id, validationKey);
    }

    prepareListToSelect(list: Array<any>) {
        return list.map(item => ({
            value: item.id,
            label: item.name
        }));
    }
}
