// @flow

import React                  from 'react';
import { bindActionCreators } from 'redux';
import { connect }            from 'react-redux';

import HeaderComponent     from '../header/header.component';
import SidebarComponent    from '../sidebar/sidebar.component';
import CompanyOfficesPage  from '../../pages/company-offices.page';
import * as actions        from '../../actions/company-offices.actions';

import 'react-select/dist/react-select.css';
import '../../../dist/css/bootstrap.min.css';
import '../../../dist/css/index.css';
import './app.component.css';


class App extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.actions.fetchData();
    }

    render() {
        const Page = CompanyOfficesPage;

        return this.props.page.loading
            ? <div>Loading...</div>
            : (
                <div className="app-container">
                    <HeaderComponent />
                    <main>
                        <div className="sidebar-root">
                            <SidebarComponent />
                        </div>
                        <div className="page-root">
                            {this.props.page.loading
                                ? <div style={({width: '100%'})}>Loading... Please wait</div>
                                : <Page pageData={this.props.page} actions={this.props.actions} />}
                        </div>
                    </main>
                </div>
            );
    }
}

export default connect(
    (state: any) => ({ page: state }),
    (dispatch: Function) => ({ actions: bindActionCreators(actions, dispatch) })
)(App);
