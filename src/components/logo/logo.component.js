// @flow

import React from 'react';
import './logo.component.css';

type styleType = {
    background?: string,
    color: string
};

export default class LogoComponent extends React.Component {
    constructor(props: { primaryColor: string, secondaryColor: string }) {
        super(props);
    }

    render() {
        const primaryStyle: styleType = {
            background: this.props.primaryColor,
            color: this.props.secondaryColor
        }
        const secondaryStyle: styleType = {
            color: this.props.primaryColor
        }


        return (
            <span className="site-logo">
                <span className="site-logo-primary" style={primaryStyle}>spd</span>
                <span className="site-logo-secondary" style={secondaryStyle}>university</span>
            </span>
        );
    }
}
