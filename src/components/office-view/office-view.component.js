// @flow

import React from 'react';
import { Glyphicon, Button } from 'react-bootstrap';

import type { officeType } from '../../store/store-types';

import './office-view.component.css';

export default class OfficeViewComponent extends React.Component {
    constructor(props: {officeData: officeType, actions: any, isPageFrozen: boolean}) {
        super(props);
    }

    render() {
        const officeData = this.props.officeData;

        return (
            <div className="office-view">
                <div className="office-view-block">
                    <div className="office-view-row">
                        <div className="office-view-title">
                            Address:
                        </div>
                        <div className="office-view-description">
                            { officeData.primary && (
                                <div>
                                    <b><Glyphicon glyph="ok" /> Primary HQ</b>
                                </div>
                            )}
                            { officeData.address2 && (
                                <div>{officeData.address2}</div>
                            )}
                            <div>{officeData.street}</div>
                            <div>{officeData.province.name}, {officeData.city.name}, {officeData['postal-code']}</div>
                            <div>{officeData.country.name}</div>
                        </div>
                    </div>
                </div>

                <div className="office-view-block">
                    { officeData.phone && (
                        <div className="office-view-row">
                            <div className="office-view-title">
                                Phone:
                            </div>
                            <div className="office-view-description">
                                {officeData.phone}
                            </div>
                        </div>
                    )}
                    { officeData.fax && (
                        <div className="office-view-row">
                            <div className="office-view-title">
                                Fax:
                            </div>
                            <div className="office-view-description">
                                {officeData.fax}
                            </div>
                        </div>
                    )}
                    { officeData.email && (
                        <div className="office-view-row">
                            <div className="office-view-title">
                                Email:
                            </div>
                            <div className="office-view-description">
                                {officeData.email}
                            </div>
                        </div>
                    )}
                </div>

                <div className="office-view-block">
                    <div className="pull-right">
                        <Button
                            bsSize="small"
                            onClick={() => this.onDeleteClick()}
                            disabled={this.props.isPageFrozen} >
                            Remove
                        </Button>
                        <Button
                            bsSize="small"
                            bsStyle="primary"
                            onClick={() => this.onEditClick()}
                            disabled={this.props.isPageFrozen} >
                            Edit
                        </Button>
                    </div>
                </div>
            </div>
        );
    }

    onDeleteClick() {
        if (this.props.isPageFrozen) {
            return;
        }

        const officeData = this.props.officeData;
        this.props.actions.officeConfirmDelete(officeData.id);
    }

    onEditClick() {
        if (this.props.isPageFrozen) {
            return;
        }

        const officeData = this.props.officeData;
        this.props.actions.officeEdit(officeData.id);
    }
}
