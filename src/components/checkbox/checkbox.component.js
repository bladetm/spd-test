// @flow

import React from 'react';
import { Glyphicon } from 'react-bootstrap';

import './checkbox.component.css';


export default class SimpleCheckboxComponent extends React.Component {
    constructor(props: {label: string, value: boolean, onChange?: Function}) {
        super(props);
    }

    render() {
        const checkboxClasses    = ['simple-checkbox'];
        const onChange: Function = this.props.onChange || (() => {});

        if (this.props.value) {
            checkboxClasses.push('checked');
        }

        return (
            <label className={checkboxClasses.join(' ')}>
                <span className="simple-checkbox-icon">
                    <input
                        className="sr-only"
                        type="checkbox"
                        checked={this.props.value}
                        onChange={e => onChange(e)} />
                    <Glyphicon glyph="ok" />
                </span>
                {this.props.label}
            </label>
        );
    }
}
