// @flow

import React     from 'react';
import Select    from 'react-select';
import { Modal, FormControl, Button } from 'react-bootstrap';

import type { officeRemovingDataType } from '../../store/store-types';


export default class OfficeDeleteComponent extends React.Component {
    constructor (props: {data: officeRemovingDataType, actions: any, isPageFrozen: boolean}) {
        super(props);
    }

    render() {
        const data: officeRemovingDataType = this.props.data;
        let defaultValue: string = data.reason || data.reasons[0];

        defaultValue = defaultValue.replace(/\s+/g, '_');

        return (
            <Modal
                className="office-delete"
                show={data.officeId !== null}>
                <Modal.Body>
                    <p>Please tell us why you’re removing this record.</p>

                    <Select
                        clearable={false}
                        value={defaultValue}
                        options={this.prepareListToSelect(data.reasons)}
                        onChange={selectedData => this.onChangeSelect(selectedData)}
                        disabled={this.props.isPageFrozen} />

                    <div className="control-label">Notes</div>
                    <FormControl
                        componentClass="textarea"
                        value={data.notes}
                        onChange={e => this.onChangeField('notes', e.target.value)}
                        disabled={this.props.isPageFrozen} />

                    <div className="modal-buttons">
                        <Button
                            onClick={() => this.onCancelClick()}
                            disabled={this.props.isPageFrozen} >
                            Cancel
                        </Button>
                        <Button
                            bsStyle="primary"
                            className="pull-right"
                            onClick={() => this.onRemoveClick()}
                            disabled={this.props.isPageFrozen} >
                            Remove Record
                        </Button>
                    </div>
                </Modal.Body>
            </Modal>
        );
    }

    onChangeSelect(selectedData: {value: string, label: string}) {
        if (this.props.isPageFrozen) {
            return;
        }

        this.onChangeField('reason', selectedData.label);
    }

    onChangeField(key: string, value: any) {
        if (this.props.isPageFrozen) {
            return;
        }

        this.props.actions.officeConfirmDeleteUpdate({[key]: value});
    }

    onCancelClick() {
        if (this.props.isPageFrozen) {
            return;
        }

        this.props.actions.officeConfirmDeleteCancel();
    }

    onRemoveClick() {
        if (this.props.isPageFrozen) {
            return;
        }

        const data = this.props.data;

        this.props.actions.officeDelete(
            data.officeId,
            data.reason,
            data.notes
        );
    }

    prepareListToSelect(list: Array<any>) {
        return list.map(item => ({
            value: item.replace(/\s+/g, '_'),
            label: item
        }));
    }
}
