import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

import LogoComponent from '../logo/logo.component';
import './header.component.css';

export default class HeaderComponent extends React.Component {
    render() {
        return (
            <Navbar collapseOnSelect fluid className="header">
                <Navbar.Header>
                    <Navbar.Brand>
                        <a className="logo" href="#">
                            <LogoComponent primaryColor="#fff" secondaryColor="#175786" />
                        </a>
                    </Navbar.Brand>

                    <span className="page-title hidden-xs">Profile editor</span>

                    <Navbar.Toggle />
                </Navbar.Header>

                <Navbar.Collapse>
                    <Nav pullRight>
                        <NavItem href="#">Contact</NavItem>
                        <NavItem href="#">FAQs</NavItem>
                        <NavItem href="#">Save and Exit</NavItem>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
